package tamides.switcher.activities;

import android.app.Application;
import android.bluetooth.BluetoothAdapter;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.wifi.WifiManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import tamides.switcher.R;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private Application application;
    private MainActivity activity;
    private Context context;

    private Button onOffBluetooth;
    private Button onOffWifi;

    private BroadcastReceiver bluetoothReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            setBluetoothButtonState();
        }
    };

    private BroadcastReceiver wifiReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            setWifiButtonState();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        application = getApplication();
        activity = this;
        context = this;

        inflateViews();
        setListeners();
    }

    private void inflateViews() {
        onOffBluetooth = (Button) findViewById(R.id.on_off_bluetooth);
        onOffWifi = (Button) findViewById(R.id.on_off_wifi);
    }

    private void setListeners() {
        onOffBluetooth.setOnClickListener(activity);
        onOffWifi.setOnClickListener(activity);
    }

    @Override
    protected void onResume() {
        super.onResume();

        setBluetoothButtonState();
        setWifiButtonState();

        registerBroadcastReceivers();
    }

    private void setBluetoothButtonState() {
        if (isBluetoothOn()) {
            onOffBluetooth.setText(R.string.turn_off_bluetooth);
        } else {
            onOffBluetooth.setText(R.string.turn_on_bluetooth);
        }
    }

    private boolean isBluetoothOn() {
        BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

        return bluetoothAdapter.isEnabled();
    }

    private void setWifiButtonState() {
        if (isWifiOn()) {
            onOffWifi.setText(R.string.turn_off_wifi);
        } else {
            onOffWifi.setText(R.string.turn_on_wifi);
        }
    }

    private boolean isWifiOn() {
        WifiManager wifiManager = (WifiManager) application.getSystemService(WIFI_SERVICE);

        return wifiManager.isWifiEnabled();
    }

    private void registerBroadcastReceivers() {
        registerBletoothReceiver();
        registerWifiReceiver();
    }

    private void registerBletoothReceiver() {
        IntentFilter bluetoothStateFilter = new IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED);

        registerReceiver(bluetoothReceiver, bluetoothStateFilter);
    }

    private void registerWifiReceiver() {
        IntentFilter wifiStateFilter = new IntentFilter(WifiManager.WIFI_STATE_CHANGED_ACTION);

        registerReceiver(wifiReceiver, wifiStateFilter);
    }

    @Override
    protected void onDestroy() {
        unregisterReceiver(bluetoothReceiver);
        unregisterReceiver(wifiReceiver);

        super.onDestroy();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.on_off_bluetooth:
                turnOnOffBluetooth();
                break;
            case R.id.on_off_wifi:
                turnOnOffWifi();
                break;
        }
    }

    private void turnOnOffBluetooth() {
        BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

        if (isBluetoothOn()) {
            bluetoothAdapter.disable();
        } else {
            bluetoothAdapter.enable();
        }
    }

    private void turnOnOffWifi() {
        WifiManager wifiManager = (WifiManager) application.getSystemService(WIFI_SERVICE);

        if (isWifiOn()) {
            wifiManager.setWifiEnabled(false);
        } else {
            wifiManager.setWifiEnabled(true);
        }
    }
}
